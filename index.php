<?php
	require_once('animal.php');
	require_once('frog.php');
	require_once('ape.php');

	echo "RELEASE 0<br>";
	$sheep = new Animal("shaun");
	echo "<br>Animal name: ".$sheep->name."<br>"; // "shaun"
	echo "Legs : ".$sheep->legs."<br>"; // 2
	echo "Cold Blooded : ".$sheep->cold_blooded."<br><br>"; // false

	echo "RELEASE 1<br>";
	//Ape
	$sungokong = new Ape("kera sakti");
	echo "<br>Animal name: ".$sungokong->name."<br>";
	echo "Legs : ".$sungokong->legs."<br>";
	echo "Cold Blooded : ".$sungokong->cold_blooded."<br>Yell : ";
	$sungokong->yell();
		

		//Frog
	$kodok = new Frog("buduk");
	echo "<br><br>Animal name: ".$kodok->name."<br>";
	echo "Legs : ".$kodok->legs."<br>";
	echo "Cold Blooded : ".$kodok->cold_blooded."<br>Jump : "; 
	$kodok->jump() ;



?>